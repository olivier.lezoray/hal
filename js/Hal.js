
	/**
		@file Hal.js
		@date October 21, 2018
	    @author Olivier Lézoray
	    @version 1.0
	    @description
	    This Javascript file contains functions useful to make queries to the HAL webservice.
	    The HAL API is https://api.archives-ouvertes.fr
	    For instance to search a structure : 
	   		https://api.archives-ouvertes.fr/ref/structure/?q=GREYC
		To search an author : 
			https://api.archives-ouvertes.fr/ref/author/?q=lézoray

		The fields for searching are :
			https://api.archives-ouvertes.fr/docs/search/schema/fields/#fields
	**/

	/**
	 * Description. 
	 * Generates a set of <option> year tags to be included in a <select>.
	 * Each <option> corresponds to a year that can be chosen in the list.
	 * @param {int}   deb   	the starting year.
	 * @param {int}   fin   	the ending year.
	 * @param {int}   theid   the id of the select to add the generated <option> tags.
	**/
	function generateYears(deb,fin,theid){
		var i;
		var actualYear=new Date();
		actualYear=actualYear.getYear()+1900;
		var option = $('<option/>');
		option.attr({ "value": "None" }).text("Aucune année");
		$("#"+theid).append(option);
		for(i=fin;i>=deb;i--) {
			var option = $('<option/>');
			option.attr({ 'value': i }).text(i);
			$("#"+theid).append(option);
		}
	}
	/**
	 * Description. 
	 * Generates a set of <option> HAL doc types tags to be included in a <select>.
	 * Each <option> corresponds to a type of document that can be chosen in the list.
	 * @param int   theid   the id of the select to add the generated <option> tags.
	**/

	function generateDocTypes(theid){				
		var res="";
		var typesCODES=new Array("None","ART","COMM","COUV","OTHER","OUV","DOUV","PATENT","POSTER","UNDEFINED","REPORT","THESE","HDR","LECTURE");
		var typesDESCR=new Array("Selectionnez un type de document","Article dans des revues","Communication dans un congrès",
			"Chapitre d'ouvrage","Autre publication","Ouvrage (y compris édition critique et traduction)",					
			"Direction d'ouvrage, Proceedings","Brevet","Poster",
			"Pré-publication, Document de travail","Rapport",
			"Thèse","HDR","Cours");
		var i;
		for(i=0;i<typesCODES.length;i++) {
			var option = $('<option/>');
			option.attr({ 'value': typesCODES[i] }).text(typesDESCR[i]);
			$("#"+theid).append(option);
		}
	}
	/**
	 * Description. 
	 * Generates in a given element, a text containing the list of documents for an author.
	 * This makes a query to the HAL API webservice.
	 * @param {int}   	 displayid   		the id of the element ot contain the generated text.
	 * @param {String}   idHal   			the id of the author.
	 * @param {String}   firstName   		the first name of the author.
	 * @param {String}   lastName   		the last name of the author.
	 * @param {int}   	 lab 	   			the id of the lab of the author.
	 * @param {String}   selectedDocType   	the type of required documents.
	 * @param {int}  	 selectedYear   	the year of the required documents.
	**/
	function getDocuments(displayid,idHal,firstName,lastName,lab,selectedDocType,selectedYear){
		var criteria="authFullName_t:"+firstName+"+"+lastName+" and labStructId_i:"+lab+" and docType_s:"+selectedDocType;
		//var criteria="authIdHal_s:"+idHal+" and labStructId_i:"+lab+" and docType_s:"+selectedDocType;
		if(selectedYear != undefined) criteria+=" and producedDateY_i:"+selectedYear;
		var fields="label_s,thumbId_i,uri_s,docid,authFullName_s,title_s,doiId_s,producedDateY_i";
		if(selectedDocType=="ART") fields+=",journalTitle_s,volume_s,number_s,page_s";
		if(selectedDocType=="COMM") fields+=",conferenceTitle_s,page_s,city_s,country_t";
		if(selectedDocType=="COUV") fields+=",scientificEditor_s,publisher_s,bookTitle_s,page_s";
		if(selectedDocType=="OUV") fields+=",publisher_s";
  		$.get("https://api.archives-ouvertes.fr/search/",
  			{q:criteria,
  			 fl:fields,
  			 sort:"producedDateY_i desc",
  			 wt:"json",
  			 rows:500},
			function(result){
				if(!result.response) {
					$("#publications").html("");
					return;
				}
				var docs = result.response.docs;
				var output="";
				if(docs.length>0) {
					output+="<h1>"+docs.length+" Document";
					if(docs.length>1) output+="s";
					output+= "</h1>";
				}
				else output+="<h1> Aucun Document </h1>";
				for(var i=0;i<docs.length;i++){		
					output+="<div class=\"media\"><br>";
					output+="<a target=\"_blank\" class=\"pull-left\" href=\""+docs[i].uri_s+"\">";
					output+="<img style=\"width:50px;\" src=\"https://thumb.ccsd.cnrs.fr/"+docs[i].thumbId_i+"/thumb\"/><br/>";
					output+="hal-"+docs[i].docid+"</a>";
					output+="<div class=\"media-body\">";
					authors=docs[i].authFullName_s;
					for(var j=0;j<authors.length;j++){
						output+=authors[j];
						if(j<(authors.length-1)) output+=", ";
  						else output+="<br/>";
					}
					output+="<b>"+docs[i].title_s[0]+"</b><br/>";
					output+="<i>";
					if(selectedDocType=="ART") {
						if(docs[i].journalTitle_s) output+=docs[i].journalTitle_s+", ";
						if(docs[i].volume_s) output+="Vol. "+docs[i].volume_s+", ";
						if(docs[i].number_s) output+="N°. "+docs[i].number_s+", ";
						if(docs[i].page_s) output+="pp. "+docs[i].page_s+", ";
					}
					else if(selectedDocType=="COMM"){
						output+=docs[i].conferenceTitle_s+", ";
						if(docs[i].page_s) output+="pp. "+docs[i].page_s+", ";
						if(docs[i].city_s) output+=docs[i].city_s+", ";
						if(docs[i].country_t) output+=docs[i].country_t+", ";
					}
					else if(selectedDocType=="COUV"){
						//output+=docs[i].label_s+", ";
						output+=docs[i].scientificEditor_s+", ";
						output+=docs[i].bookTitle_s+", ";
						output+=docs[i].publisher_s+", ";						
						if(docs[i].page_s) output+="pp. "+docs[i].page_s+", ";
					}
					else if(selectedDocType=="OUV"){
						output+=docs[i].publisher_s+", ";	

					}
					output+=docs[i].producedDateY_i+".";
					output+="</i><br/>";
					if(docs[i].doiId_s){
						output+="<a target=\"_blank\" href=\"http://dx.doi.org/"+docs[i].doiId_s+"\">";
						output+="&lt;"+docs[i].doiId_s+"&gt;</a>";
					}
					output+="</div>";
					output+="</div>";
				}				
				$("#"+displayid).html(output);												
			},
			"json"
		);
	}
	/**
	 * Description. 
	 * Makes an update of the document on a change of first or last name.
	**/
	function nameChange(){
		if($("#lastName").length && $("#firstName").length)
			typeChange();
	}
	/**
	 * Description. 
	 * Makes an update of the document on a change of year.
	**/
	function yearChange(){
		var selectedYear=$("#yearSelect").val();
		var selectedDocType=$("#documentTypeSelect").val();
		if(selectedYear=="None")	{
			getDocuments("publications",idHal,firstName,lastName,lab,selectedDocType);
		}
		else {
			getDocuments("publications",idHal,firstName,lastName,lab,selectedDocType,selectedYear);
		}
	}
	/**
	 * Description. 
	 * Makes an update of the document on a change of document type.
	**/
	function typeChange(){
		if($("#lastName").length) lastName=$("#lastName").val();
		if($("#firstName").length) firstName=$("#firstName").val();
		var selectedDocType=$("#documentTypeSelect").val();
		if(selectedDocType=="None" || (lastName==""))	{
			$("#publications").html("");
			$("#yearSelect").hide();
			$("#yearSelectLabel").hide();
		}
		else {
			if($("#yearSelect").is(":visible")){
				var selectedYear=$("#yearSelect").val();
				if(selectedYear=="None")	{
					getDocuments("publications",idHal,firstName,lastName,lab,selectedDocType);
				}
				else getDocuments("publications",idHal,firstName,lastName,lab,selectedDocType,selectedYear);
			}
			else {
				getDocuments("publications",idHal,firstName,lastName,lab,selectedDocType);
				$("#yearSelect").show();
				$("#yearSelectLabel").show();			
			}
		}
  	}