## Description
Outil d'interrogation du web service HAL

## Principe
Pour les chercheurs, un dépôt de leur travaux sur HAL (https://hal.archives-ouvertes.fr) est souvent necéssaire
et permet une bonne indexation de leurs travaux de recherche.
Ceci permet également de disposer d'une liste à jour de leurs travaux de recherche et qui peut être triée selon différents critères (type de publication, année de publication).
L'outil proposé permet d'utiliser de manière transparente l'API de HAL via l'appel à la fonction JavaScript suivante
```javascript
function getDocuments(displayid,idHal,firstName,lastName,lab,selectedDocType,selectedYear)
```
dont les paramètres sont
1. L'élément HTML qui permettra d'afficher les documents
2. L'id HAL de l'auteur
3. Le prénom de l'auteur
4. Le nom de l'auteur
5. l'identifiant HAL du laboratoire de l'auteur
6. Le type de document demandé
7. L'année des documents demandés

Le fichier *publicationsHAL.html* est un exemple d'utilisation pour un auteur specifique.
Le fichier *publicationsHALParPersonne.html* est un exemple d'utilisation pour lequel on dispose de champs de saisie des nom/prénom de l'auteur.

Des fonctions JavaScript sont également fournies pour faciliter l'utilisation de ces exemples.

## Exemples d'utilisation
### Pour un auteur spécifique
![Demo un auteur](https://git.unicaen.fr/olivier.lezoray/hal/raw/master/img/DemoOneAuthor.gif)
### Pour un auteur dont les nom/prénom sont à saisir
![Demo auteur à choisir](https://git.unicaen.fr/olivier.lezoray/hal/raw/master/img/DemoAnyAuthor.gif)
